<?php
include_once '../Core/DbCrud.php';
include_once '../Core/Access.php';
include_once '../Core/DbContext.php';
include_once '../Models/user.php';

$Connection = new Access($conn);
$model = new user();
if($_POST['input'] == "register")
	{
		$Username =  mysql_real_escape_string($_POST['Username']);
		$user = $Connection->getByUsername($Username);
		if(isset($user['Id']))
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"user available")));
		}
		else
		{
			$Connection = new DbCrud($conn, 'users');
			$model->Id = '';
			$model->Full_Name = mysql_real_escape_string($_POST['Full_Name']);
			$model->Address = mysql_real_escape_string($_POST['Address']);
			$model->Contact = mysql_real_escape_string($_POST['Contact']);
			$model->Photo = '';
			$model->Email = mysql_real_escape_string($_POST['Email']);
			$model->Username = $Username;
			$model->Password = md5(mysql_real_escape_string($_POST['Password']));
			$action = $Connection->Save($model);
			if($action)	
			{
				$model->Password = "";
				echo(json_encode($model));
			}
			else
			{
				header("HTTP/1.1 400 Bad Request");
				
				echo(json_encode(array("Message"=>"registering fail")));
			}
		}
}
else if($_POST['input'] == "login")
{
	$Username =  mysql_real_escape_string($_POST['Username']);
	$user = $Connection->getByUsername($Username);
	if(!isset($user['Id']))
	{
		header("HTTP/1.1 400 Bad Request");
		echo(json_encode(array("Message"=>"user unavailable")));
	}
	else
	{
		$Password = md5(mysql_real_escape_string($_POST['Password']));
		if($user['Password'] == $Password)
		{
			$Connection->sessionSet($user);
			$user["Message"] = "login success";
			$user["Password"] = "";
			echo(json_encode($user));
		}
		else
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"wrong password")));
		}
	}
	
}
elseif(isset($_GET['logout']))
{
	$Connection->sessionUnSet();
}
else
{
	header("HTTP/1.1 400 Bad Request");
	echo(json_encode(array("Message"=>"wrong method")));
}
?>