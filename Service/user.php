<?php
include_once '../Core/DbCrud.php';
include_once '../Core/DbContext.php';
include_once '../Models/user.php';

$Connection = new DbCrud($conn, 'users');
$model = new user();

if (isset($_SESSION['Token'])) {
	if(mysql_real_escape_string(isset($_POST['input'])))
	{
		$model->Id = '';
		$model->Full_Name = mysql_real_escape_string($_POST['Full_Name']);
		$model->Address = mysql_real_escape_string($_POST['Address']);
		$model->Contact = mysql_real_escape_string($_POST['Contact']);
		$model->Photo = '';
		$model->Email = mysql_real_escape_string($_POST['Email']);
		$model->Username = mysql_real_escape_string($_POST['Username']);
		$model->Password = mysql_real_escape_string($_POST['Password']);
		$action = $Connection->Save($model);
		if($action)
		{
			$model =(array)$model;
			$model['Message'] = "Success";
			echo(json_encode($model));
		}
		else
		{
			echo('Error');
		}
	}
	elseif (mysql_real_escape_string(isset($_POST['update'])))
	{
		$model->Id = $_SESSION['Id'];
		$model->Full_Name = mysql_real_escape_string($_POST['Full_Name']);
		$model->Address = mysql_real_escape_string($_POST['Address']);
		$model->Contact = mysql_real_escape_string($_POST['Contact']);
		$model->Photo = '';
		$model->Email = mysql_real_escape_string($_POST['Email']);
		$model->Username = mysql_real_escape_string($_POST['Username']);
		$model->Password = md5(mysql_real_escape_string($_POST['Password']));
		$action = $Connection->Update($model);
		if($action)
		{
			$model =(array)$model;
			$model['Message'] = "Success";
			echo(json_encode($model));
		}
		else
		{
			echo('Error.');
		}		
	}
	elseif (mysql_real_escape_string(isset($_GET['delete']))) 
	{
		
		$action = $Connection->Delete(mysql_real_escape_string($_GET['Id']));
		if($action)
		{
			echo('Success.');
		}
		else
		{
			echo('Error.');
		}		
	}
	elseif (mysql_real_escape_string(isset($_GET['Id']))) 
	{
		
		$action = $Connection->showDetail($_SESSION['Id']);
		if($action)
		{
			echo(json_encode($action));
		}
		else
		{
			echo('Error.');
		}		
	}
	else
	{
		$action = $Connection->showAll();
		if($action)
		{
			echo(json_encode($action));
		}
		else
		{
			echo('Error.');
		}		
	}
}
else
{
	header("HTTP/1.1 400 Bad Request");
	echo("Access denied");
}
?>