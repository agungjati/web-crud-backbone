<?php
include_once '../Core/DbCrud.php';
include_once '../Core/DbContext.php';
include_once '../Models/post.php';

$Connection = new DbCrud($conn, 'posts');
$model = new post();

if (isset($_SESSION['Token'])) {
	if(isset($_POST['input']))
	{
		$model->Id = '';
		$model->Title = mysql_real_escape_string($_POST['Title']);
		$model->Image = '';
		$model->Content = mysql_real_escape_string($_POST['Content']);
		$model->Creator = $_SESSION['Id'];
		$action = $Connection->Save($model);
		if($action)
		{
			$model =(array)$model;
			$model['Message'] = "Success";
			echo(json_encode($model));	
		}
		else
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"failed")));
		}
	}
	elseif (isset($_POST['update'])) 
	{
		$model->Id = mysql_real_escape_string($_GET['Id']);
		$model->Title = mysql_real_escape_string($_POST['Title']);
		$model->Content = mysql_real_escape_string($_POST['Content']);
		$model->Image = '';
		$model->Creator = $_SESSION['Id'];
		$action = $Connection->Update($model);
		if($action)
		{
			$model =(array)$model;
			$model['Message'] = "Success";
			echo(json_encode($model));
		}
		else
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"failed")));
		}		
	}
	elseif (isset($_GET['delete'])) 
	{
		
		$action = $Connection->Delete(mysql_real_escape_string($_GET['Id']));
		if($action)
		{
			echo(json_encode(array("Message"=>"Success")));
		}
		else
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"failed")));
		}		
	}
	elseif (isset($_GET['Id']))
	{
		
		$action = $Connection->showDetail(mysql_real_escape_string($_GET['Id']));
		if($action)
		{
			echo(json_encode($action));
		}
		else
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"failed")));
		}		
	}
	else
	{
		include_once '../Core/Access.php';
		$Connection = new Access($conn);
		$action = $Connection->getPostByID($_SESSION['Id']);
		if($action)
		{
			echo(json_encode($action));
		}
		else
		{
			header("HTTP/1.1 400 Bad Request");
			echo(json_encode(array("Message"=>"failed")));
		}		
	}
}
else
{
	header("HTTP/1.1 400 Bad Request");
	echo(json_encode(array("Message"=>"Access denied")));
}
?>
