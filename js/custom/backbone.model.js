define(function(require, exports, module) {
    'use strict';
    require('deep-model');

    module.exports = Backbone.DeepModel.extend({
        initialize: function(options) {
            if (this.beforeInitialize) {
                this.beforeInitialize(options);
            }

            this.on('request', function() {
                this.requestToServer = true
            });

            this.on('sync error', function() {
                this.requestToServer = false
            });

            this.on('error', function(model, xhr) {
                if (xhr != 200) {
                    let data = xhr.responseJSON
                    if (data) {
                        if (data.error_description || data.Message) {
                            alert(data.error_description || data.Message)
                        } else {
                            alert(data)
                        }
                    }
                }
            });
        }
    });
});