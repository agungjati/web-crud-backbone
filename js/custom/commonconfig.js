define(function(require, exports, module) {
    module.exports = {
        requestServer: window.location.hostname == 'localhost' ? 'http://localhost/' : 'http://api.logistical.onebyone.co.id/api/',
        requestServerNotAPI: window.location.hostname == 'localhost' ? 'http://localhost/' : 'http://api.logistical.onebyone.co.id/',
    }
});
