define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var Model = require('./../js/custom/Dashboard_up/model');
    var template = require('text!./../js/custom/Dashboard_up/template.html');

    module.exports = LayoutManager.extend({
//        tagName: '',
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.listenTo(this.model, 'sync', function() {
                window.location = '#';
            });
        },
        events: {
            //'click a[name="goback"]': 'goback',
            //'click a[name="logout"]': 'logout'
        },
        /*goback: function() {
            window.history.back();
        },*/
        logout: function() {
            this.model.save();
        }
    });
});