define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./../js/custom/footer/template.html');
    var Cookies = require('Cookies');

    module.exports = LayoutManager.extend({
        tagName: 'footer',
        className: 'bg-dark p-4 container-fluid',
        attributes: {
            //style: 'background:#fff; box-shadow:0 2px 3px #ccc'
        },
        template: _.template(template)
    });
});