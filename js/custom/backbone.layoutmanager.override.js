define(function(require, exports, module) {
    'use strict';
    let LayoutManager = require('layoutmanager.original');
    module.exports = LayoutManager.extend();
});