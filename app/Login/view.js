define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var Cookies = require('Cookies');

    module.exports = LayoutManager.extend({
    	className:'',
    	attributes:{
    		//'style':'margin-top:65px; margin-bottom:75px;'
    	},
    	template: _.template(template),
        events:{
            'submit form': 'submit'
        },
        submit: function(event){
            event.preventDefault();
            $.ajax({
                method:"POST",
                url: "http://localhost/simpleweb-use-backbone/Service/access_acount.php",
                data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                    Cookies.set("Username", msg.Username);
                    Cookies.set("Name", msg.Full_Name);
                    window.location.hash='#Dashboards';
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message);
                    else
                        alert(msg);
                }
            });
    	}
    });
});