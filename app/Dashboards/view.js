define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    
    module.exports = LayoutManager.extend({
    	className:'',
    	attributes:{
    		//'style':'margin-top:65px; margin-bottom:75px;'
    	},
    	template: _.template(template),
    	events:{
    		'click a[name="logout"]': 'submit'
    	},
    	submit: function(event){
    		event.preventDefault();
    		$.ajax({
                method:"POST",
                url: "http://localhost/simpleweb-use-backbone/Service/access_acount.php?logout=gung",
                //data: $(event.currentTarget).serializeArray(),
                //dataType:"json",
                success :function(msg)
                {
                	window.location.hash='#';
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
    	}
    });
});