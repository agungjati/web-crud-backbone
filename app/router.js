//scripts/app
define(function(require, exports, module) {
    'use strict';

    let Backbone = require('backbone');
    let commonFunction = require('commonfunction');

    let fnSetContentView = function(pathViewFile, options) {
        require(['./' + pathViewFile + '/view'], function(View) {
            commonFunction.setContentViewWithNewModuleView(new View(), options);
        });
    };

    module.exports = Backbone.Router.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            '':'showHome',
            'Login':'showLogin',
            'Register':'showRegister',
            'Dashboards':'showDashboard',
            'Post':'showPost',
            'detailPost':'showDetailPost',
            'updatePost/:id':'updatePost',
            'Guest_book':'showGuest_book',
            'detailBook':'showDetailBook',
            'updateBook/:id':'updateBook',
            'Setting':'showSetting',
            'detailUser':'showDetailSetting',
            'about':'',
            '*actions': 'notFound'
        },
        start: () => {
            Backbone.history.start();
        },
        showHome:() => {
            fnSetContentView('Home', {
                showTopBottomView: "TopBottom"
            });
        },
        showRegister: () =>{
            fnSetContentView('./Register', {
                showTopBottomView: ""
            });
        },
        showLogin: () =>{
            fnSetContentView('./Login', {
                showTopBottomView: ""
            });
        },
        showDashboard:() =>{
            fnSetContentView('./Dashboards', {
                showTopBottomView: ""
            }); 
        },
        showPost:() =>{
            fnSetContentView('./Post', {   
                showTopBottomView: ""
            }); 
        },
        showDetailPost:() =>{
            fnSetContentView('./Post/detail', {   
                showTopBottomView: ""
            }); 
        },
        updatePost:() =>{
            fnSetContentView('./Post/update', {   
                showTopBottomView: ""
            }); 
        },
        showGuest_book:() =>{
            fnSetContentView('./Guest_book', {   
                showTopBottomView: ""
            }); 
        },
        showDetailBook:() =>{
            fnSetContentView('./Guest_book/detail', {   
                showTopBottomView: ""
            }); 
        },
        updateBook:() =>{
            fnSetContentView('./Guest_book/update', {   
                showTopBottomView: ""
            }); 
        },
        showSetting:() =>{
            fnSetContentView('./setting', {   
                showTopBottomView: ""
            }); 
        },
        showDetailSetting:() =>{
            fnSetContentView('./setting/detail', {   
                showTopBottomView: ""
            }); 
        },
        notFound: () => {
            alert('sorry not have what you want');
        }
    });
});