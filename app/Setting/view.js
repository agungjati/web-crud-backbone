define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    
    module.exports = LayoutManager.extend({
    	className:'',
    	attributes:{
    		//'style':'margin-top:65px; margin-bottom:75px;'
    	},
    	template: _.template(template),
    	afterRender:function(){
            $.ajax({
                method:"GET",
                url: "http://localhost/simpleweb-use-backbone/Service/user.php?Id=gung",
                //data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                    $("input[name='Full_Name']").val(msg.Full_Name);
                    $("input[name='Address']").val(msg.Address);
                    $("input[name='Contact']").val(msg.Contact);
                    $("input[name='Email']").val(msg.Email);
                    $("input[name='Username']").val(msg.Username);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
        },
        events:
        {
            'submit form':'submit'
        },
        submit:function(event)
        {
            event.preventDefault();
            $.ajax({
                method:"POST",
                url: "http://localhost/simpleweb-use-backbone/Service/user.php",
                data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                    alert(msg.Message);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
        }

    });
});
