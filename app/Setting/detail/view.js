define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    
    module.exports = LayoutManager.extend({
    	className:'',
    	attributes:{
    		//'style':'margin-top:65px; margin-bottom:75px;'
    	},
    	template: _.template(template),
        afterRender: function(){
            load();
        }
    });
});
function load()
{
    $.ajax({
                method:"GET",
                url: "http://localhost/simpleweb-use-backbone/Service/user.php",
                //data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                    var text = "";
                    for(var i=0; i<msg.length; i++)
                    {
                        var x = i+1;
                        text += ` <tr>
                        <td>`+ x +`</td>
                        <td>`+ msg[i].Full_Name +`</td>
                        <td>`+ msg[i].Username +`</td>
                        <td>`+ msg[i].Address +`</td>
                        <td>`+ msg[i].Contact +`</td>
                        </tr>`;
                    }
                    $("#fill").html(text);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
}
