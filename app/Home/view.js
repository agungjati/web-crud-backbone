define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    
    module.exports = LayoutManager.extend({
    	className:'',
    	attributes:{
    		//'style':'margin-top:65px; margin-bottom:75px;'
    	},
    	template: _.template(template),
    	events:{
    		'submit form': 'submit'
    	},
    	submit: function(event){
    		event.preventDefault();
    		$.ajax({
                method:"POST",
                url: "http://localhost:50981/api/Account/Register",
                data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                	alert(`thanks for signing up, you'll be back to login form `);
                	window.location.hash='login';
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message);
                    else
                        alert(msg);
                }
            });
    	}
    });
});