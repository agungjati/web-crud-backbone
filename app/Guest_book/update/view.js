define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    
    module.exports = LayoutManager.extend({
        className:'',
        attributes:{
            //'style':'margin-top:65px; margin-bottom:75px;'
        },
        template: _.template(template),
        afterRender: function(){
            $.ajax({
                method:"POST",
                url: "http://localhost/simpleweb-use-backbone/Service/guest_book.php?Id="+ GetURLParameter(),
               //data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                   $("input[name='Name']").val(msg.Name);
                   $("input[name='Email']").val(msg.Email);
                   $("input[name='Address']").val(msg.Address);
                   $("textarea[name='Message']").val(msg.Message);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
        },
        events:{
            'submit form': 'submit'
        },
        submit: function(event){
            event.preventDefault();
            $.ajax({
                method:"POST",
                url: "http://localhost/simpleweb-use-backbone/Service/guest_book.php?Id="+ GetURLParameter(),
                data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                    alert(msg.Message);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
        }
    });
});

function GetURLParameter() {
                var sPageURL = window.location.href;
                var indexOfLastSlash = sPageURL.lastIndexOf("/");

                if (indexOfLastSlash > 0 && sPageURL.length - 1 != indexOfLastSlash)
                    return sPageURL.substring(indexOfLastSlash + 1);
                else
                    return 0;
            }