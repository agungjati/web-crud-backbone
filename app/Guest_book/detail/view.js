define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    
    module.exports = LayoutManager.extend({
    	className:'',
    	attributes:{
    		//'style':'margin-top:65px; margin-bottom:75px;'
    	},
    	template: _.template(template),
        afterRender: function(){
            load();
        },
        events:
        {
            'click a[name="hapus"]':'hapus'
        },
        hapus:function(e)
        {
            var id = $(e.currentTarget).attr('value');

             $.ajax({
                method:"GET",
                url: "http://localhost/simpleweb-use-backbone/Service/guest_book.php?delete=a&Id="+id,
                //data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                   alert(msg.Message);
                   load();
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
        }
    });
});
function load()
{
    $.ajax({
                method:"GET",
                url: "http://localhost/simpleweb-use-backbone/Service/guest_book.php",
                //data: $(event.currentTarget).serializeArray(),
                dataType:"json",
                success :function(msg)
                {
                    var text = "";
                    for(var i=0; i<msg.length; i++)
                    {
                        text += ` <tr>
                        <td>`+ msg[i].Name +`</td>
                        <td>`+ msg[i].Email +`</td>
                        <td>`+ msg[i].Address +`</td>
                        <td>`+ msg[i].Message +`</td>
                        <td>
                            <a class="btn btn-sm btn-outline-primary" value="`+ msg[i].Id +`" name="hapus">hapus</a>
                            <a class="btn btn-sm btn-outline-primary" href="#updateBook/`+ msg[i].Id +`" name="update">update</a>
                        </td>
                        </tr>`;
                    }
                    $("#fill").html(text);
                },
                error: function (xhr, msg) {
                    if (xhr.responseJSON && xhr.responseJSON.Message)
                        alert(xhr.responseJSON.Message || msg);
                    else
                        alert(msg);
                }
            });
}
