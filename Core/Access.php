<?php
class Access
{
	private $db;
	public function __construct($conn)
	{
		$this->db = $conn;
		try
		{
			$fetch = $this->db->prepare("SELECT * FROM users");
			$fetch->execute();	
		}
		catch (PDOException $e) 
		{
			echo("Error ! : ". $e->getMessage());
		}	
	}

	public function getByUsername($username)
	{
		try 
		{
			$fetching = $this->db->prepare("SELECT * FROM users WHERE Username=:username");
			$fetching->bindParam(":username", $username);
			$fetching->execute();
			$data = array();
			while ($row = $fetching->fetch(PDO::FETCH_ASSOC)) {
				foreach ($row as $key => $value) {
					$data[$key] =  $value;
				}
			}
			return $data;
		} 
		catch (PDOException $e) 
		{
			echo("Error ! : ". $e->getMessage());
		}		
	}

	public function getPostByID($id)
	{
		try 
		{
			$fetching = $this->db->prepare("SELECT * FROM posts WHERE Creator=:id");
			$fetching->bindParam(":id", $id);
			$fetching->execute();
			$data = array();
			while ($rows = $fetching->fetch(PDO::FETCH_ASSOC)) {
				$_data = array();
				foreach ($rows as $row => $value) {
		  			$_data[$row] = $value;
		  		} 
		  		array_push($data, $_data);
		 	}
			return $data;
		} 
		catch (PDOException $e) 
		{
			echo("Error ! : ". $e->getMessage());
		}		
	}

	public function sessionSet($values)
	{
		date_default_timezone_set('Asia/Jakarta');
 		$date = date("Y-m-d H:i:s");
		$_SESSION['username'] = $values['Username'];
		$_SESSION['date'] = $date;
		$_SESSION['Id'] = $values['Id'];
		$_SESSION['Token'] = md5($values['Id']);
	}

	public function sessionUnSet()
	{
		session_unset();
		session_destroy();
	}
}
?>